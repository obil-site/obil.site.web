﻿using Dto;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;

namespace WebApi
{
    /// <summary>
    /// FilterConfig
    /// obil.site.web
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// 全局过滤器注册
        /// obil.site.web
        /// </summary>
        /// <param name="mvcFilters">GlobalFilters.Filters</param>
        /// <param name="apiFilters">GlobalConfiguration.Configuration.Filters</param>
        internal static void RegisterGlobalFilters(GlobalFilterCollection mvcFilters, HttpFilterCollection apiFilters)
        {
            mvcFilters.Add(new MvcHandleErrorAttribute());

            apiFilters.Add(new ApiExceptionFilterAttribute());

            apiFilters.Add(new ApiActionFilterAttribute());
        }
    }
    /// <summary>
    /// ApiActionFilterAttribute
    /// obil.site.web
    /// </summary>
    public class ApiActionFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// OnActionExecuted
        /// obil.site.web
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            //记录日志
            if (actionExecutedContext.Response != null)
            {
                var api = actionExecutedContext.ActionContext.ControllerContext.Controller as ApiController;
                if (actionExecutedContext.Response.TryGetContentValue(out object ResultContent))
                {
                    WebGlobalConfig.Config.TriggerOnWebApiResponsed(new WebApiInfo()
                    {
                        HttpMethod = actionExecutedContext.Request.Method.Method,
                        AbsolutePath = actionExecutedContext.Request.RequestUri.AbsolutePath,
                        UserName = api.User.Identity.Name ?? string.Empty,
                        UserIPAddress = actionExecutedContext.Request.GetClientIPAddress(),
                        UserAgent = actionExecutedContext.Request.Headers.UserAgent.ToString(),
                        ActionArguments = actionExecutedContext.ActionContext.ActionArguments ?? new Dictionary<string, object>(),
                        ResultContent = ResultContent,
                        RequestTime = (api.User.Identity as ApiIdentity)?.RequestTime ?? DateTime.Now
                    });
                }
            }
            base.OnActionExecuted(actionExecutedContext);
        }
        /// <summary>
        /// 
        /// obil.site.web
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.UserAgent == null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest);
                base.OnActionExecuting(actionContext);
                return;
            }
            if (actionContext.Request.Method == HttpMethod.Options)//跨域
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Accepted);
                base.OnActionExecuting(actionContext);
                return;
            }
            if (!actionContext.ModelState.IsValid)//非法参数
            {
                using (var result = new ApiResultBuilder<string>())
                {
                    var msg = actionContext.ModelState.Values.SelectMany(w => w.Errors).Select(q => q.ErrorMessage).FirstOrDefault() ?? "错误参数";
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, result.Error1406(msg));
                }
                base.OnActionExecuting(actionContext);
                return;
            }
            if (actionContext.ActionDescriptor.GetParameters().Any(q => q.ParameterBinderAttribute is FromUriAttribute && actionContext.ActionArguments[q.ParameterName] == null))//缺少参数(复杂类型)
            {
                var arg = actionContext.ActionDescriptor.GetParameters().First(q => q.ParameterBinderAttribute is FromUriAttribute
                && actionContext.ActionArguments[q.ParameterName] == null);
                var urlarg = $@"{string.Join("&", arg.ParameterType.GetProperties().Where(w => w.CanWrite).Select(w => $"{w.Name}="))}";
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.TemporaryRedirect);//永久重定向
                actionContext.Response.Headers.Add("Location", $"{actionContext.Request.RequestUri}?{urlarg}");//填充参数名
                base.OnActionExecuting(actionContext);
                return;
            }
            var api = actionContext.ControllerContext.Controller as ApiController;
            //身份验证
            if (actionContext.Request.Headers.Authorization?.Scheme == new ApiIdentity().AuthenticationType)
            {
                string token = actionContext.Request.Headers.Authorization?.Parameter ?? string.Empty;
                if (string.IsNullOrWhiteSpace(token))
                    api.User = new ApiPrincipal() { Identity = new ApiIdentity() };
                try
                {
                    if (WebGlobalConfig.Config.EnableRedisAuthorizationCache)
                    {
                        using (var client = new RedisClient(WebGlobalConfig.Config.RedisConnectionString))
                        {
                            if (client.ContainsKey(token))
                                token = client.Get<string>(token);
                        }
                    }
                    var apiApiIdentity = token.AES128Decryption().JsonDeserialize<ApiIdentity>();
                    if (apiApiIdentity.Exipre >= DateTime.Now.ToUnixTimestamp())
                    {
                        apiApiIdentity.Token = token;
                        api.User = new ApiPrincipal() { Identity = apiApiIdentity };
                    }
                    else
                    {
                        api.User = new ApiPrincipal() { Identity = new ApiIdentity() };
                    }
                }
                catch
                {
                    api.User = new ApiPrincipal() { Identity = new ApiIdentity() };
                }
            }
            else
                api.User = new ApiPrincipal() { Identity = new ApiIdentity() };

            if (actionContext.ActionDescriptor.GetCustomAttributes<ApiIdentityFilterAttribute>().Any() && !api.User.Identity.IsAuthenticated)
            {
                //身份验证失败
                using (var result = new ApiResultBuilder<string>())
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, result.Error1401());
                }
                base.OnActionExecuting(actionContext);
                return;
            }
            if (actionContext.ActionDescriptor.GetCustomAttributes<ApiIdentityFilterAttribute>().Any()
                && !api.User.IsInRole(actionContext.ActionDescriptor.GetCustomAttributes<ApiIdentityFilterAttribute>().FirstOrDefault()?.AuthCode))
            {
                //权限验证失败
                using (var result = new ApiResultBuilder<string>())
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, result.Error1403());
                }
                base.OnActionExecuting(actionContext);
                return;
            }
            base.OnActionExecuting(actionContext);
        }
    }
    /// <summary>
    /// ApiExceptionFilterAttribute
    /// obil.site.web
    /// </summary>
    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// OnException
        /// obil.site.web
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            WebGlobalConfig.Config.TriggerOnException(actionExecutedContext.Exception);
            if (actionExecutedContext.Exception is HttpResponseException)
            {
                var httpResponseException = actionExecutedContext.Exception as HttpResponseException;
                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(new ApiResult<string>()
                {
                    code = 1500,
                    msg = $"{httpResponseException.Response.StatusCode}:{httpResponseException.Response.ReasonPhrase}"
                });
            }
            else if (actionExecutedContext.Exception is DbEntityValidationException)
            {
                var dbEntityValidationException = actionExecutedContext.Exception as DbEntityValidationException;
                var entityValidationErrors = dbEntityValidationException.EntityValidationErrors
                    .SelectMany(q => q.ValidationErrors.Select(w => $"{w.PropertyName}:{w.ErrorMessage}"));
                WebGlobalConfig.Config.TriggerOnException(new Exception($"EntityValidationErrors:{string.Join(",", entityValidationErrors)}"));
                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(new ApiResult<string>()
                {
                    code = 1500,
                    msg = $"{500}:数据库繁忙!"
                });
            }
            else
            {
                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(new ApiResult<string>()
                {
                    code = 1500,
                    msg = $"{500}:服务器繁忙!"
                });
            }
        }
    }
    /// <summary>
    /// MvcHandleErrorAttribute
    /// obil.site.web
    /// </summary>
    public class MvcHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            WebGlobalConfig.Config.TriggerOnException(filterContext.Exception);
            base.OnException(filterContext);
        }
    }

}
