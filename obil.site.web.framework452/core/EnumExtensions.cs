﻿using System;
using System.Collections.Generic;
using System.Text;

namespace obil.site.core
{
    /// <summary>
    /// EnumExtensions
    /// obil.site.core
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// 转换为枚举值
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int ToEnumValue(this Enum obj)
        {
            return Convert.ToInt32(obj);
        }
    }
}
