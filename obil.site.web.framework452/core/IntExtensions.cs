﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    /// <summary>
    /// IntExtensions
    /// obil.site.core
    /// </summary>
    public static class IntExtensions
    {
        /// <summary>
        /// 转换为枚举类名称
        /// obil.site.core
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToEnumName<T>(this int obj) where T : Enum
        {
            return Enum.GetName(typeof(T), obj);
        }
    }
}
