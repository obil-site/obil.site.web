﻿namespace System
{
    /// <summary>
    /// DecimalExtensions
    /// obil.site.core
    /// </summary>
    public static class DecimalExtensions
    {
        /// <summary>
        /// 转换为保留两位小数字符串
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToF2String(this decimal obj)
        {
            return obj.ToString("f2");
        }
        /// <summary>
        /// 转换为保留两位小数字符串
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToF2String(this decimal? obj)
        {
            if (obj.HasValue)
                return obj.Value.ToString("f2");
            return string.Empty;
        }
    }
}
