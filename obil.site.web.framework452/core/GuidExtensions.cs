﻿

namespace System
{
    /// <summary>
    /// GuidExtensions
    /// obil.site.core
    /// </summary>
    public static class GuidExtensions
    {
        /// <summary>
        /// 转换为32位长度的字符串(N)
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToNString(this Guid obj)
        {
            return obj.ToString("N");
        }
    }
}
