﻿using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    /// <summary>
    /// StringExtensions
    /// obil.site.core
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// 获取值或String.Empty
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetValueOrEmpty(this string obj)
        {
            return obj.GetValueOrDefualt(string.Empty);
        }
        /// <summary>
        /// JSON反序列化
        /// obil.site.core
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T JsonDeserialize<T>(this string obj)
        {
            return JsonConvert.DeserializeObject<T>(obj);
        }
        /// <summary>
        /// 作为Base64转换为字节
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static byte[] Base64ToBytes(this string obj)
        {
            return Convert.FromBase64String(obj);
        }
        /// <summary>
        /// 作为文件路径读取全部文件字符串
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string FileNameToReadAllText(this string obj, string encoding = "utf-8")
        {
            return File.ReadAllText(obj, Encoding.GetEncoding(encoding));
        }
        /// <summary>
        /// 作为文件路径写入全部文件字符串
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static void FileNameToWriteAllText(this string obj, string content, string encoding = "utf-8")
        {
            File.WriteAllText(obj, content, Encoding.GetEncoding(encoding));
        }

        /// <summary>
        /// 作为文本转换为字节
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static byte[] ToTextBytes(this string obj, string encoding = "utf-8")
        {
            return Encoding.GetEncoding(encoding).GetBytes(obj);
        }
        /// <summary>
        /// MD5加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string MD5Encryption(this string obj, string encoding = "utf-8")
        {
            return obj.ToTextBytes(encoding).MD5Encryption();
        }
        /// <summary>
        /// 作为文本进行SHA1加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string SHA1Encryption(this string obj, string encoding = "utf-8")
        {
            return obj.ToTextBytes(encoding).SHA1Encryption();
        }
        /// <summary>
        /// 作为文本进行SHA256加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string SHA256Encryption(this string obj, string encoding = "utf-8")
        {
            return obj.ToTextBytes(encoding).SHA256Encryption();
        }
        /// <summary>
        /// 作为文本进行SHA384加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string SHA384Encryption(this string obj, string encoding = "utf-8")
        {
            return obj.ToTextBytes(encoding).SHA384Encryption();
        }
        /// <summary>
        /// 作为文本进行SHA512加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string SHA512Encryption(this string obj, string encoding = "utf-8")
        {
            return obj.ToTextBytes(encoding).SHA512Encryption();
        }
        /// <summary>
        /// 作为文本进行AES128加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="key">密码</param>
        /// <param name="iv">向量</param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string AES128Encryption(this string obj, string key, byte[] iv, string encoding = "utf-8")
        {
            var encodedData = obj.ToTextBytes(encoding).AES128Encryption(key.ToTextBytes(encoding), iv);
            return encodedData.ToBase64String();
        }
        /// <summary>
        /// 作为文本进行AES128加密(默认向量)
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="key">密码</param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string AES128Encryption(this string obj, string key = "obil.site.core", string encoding = "utf-8")
        {
            return obj.AES128Encryption(key, new byte[] { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF }, encoding);
        }
        /// <summary>
        /// 作为文本进行AES128解密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="key">密码</param>
        /// <param name="iv">向量</param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string AES128Decryption(this string obj, string key, byte[] iv, string encoding = "utf-8")
        {
            var decodedData = obj.Base64ToBytes().AES128Decryption(key.ToTextBytes(encoding), iv);
            return decodedData.ToTextString(encoding);
        }
        /// <summary>
        /// 作为文本进行AES128解密(默认向量)
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="key">密码</param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string AES128Decryption(this string obj, string key = "obil.site.core", string encoding = "utf-8")
        {
            return obj.AES128Decryption(key, new byte[] { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF }, encoding);
        }
    }
}
