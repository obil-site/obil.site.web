﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    /// <summary>
    /// 数据映射接口
    /// obil.site.core
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMapTo<T>
    {
        /// <summary>
        /// 数据映射到
        /// obil.site.core
        /// </summary>
        /// <param name="target">目标对象</param>
        void MapTo(T target);
    }
}
