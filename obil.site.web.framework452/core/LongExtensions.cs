﻿namespace System
{
    /// <summary>
    /// LongExtensions
    /// obil.site.core
    /// </summary>
    public static class LongExtensions
    {
        /// <summary>
        /// 转换为日期时间(Unix时间戳)
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static DateTime ToUnixDateTime(this long obj)
        {
            return new DateTime(621355968000000000).ToLocalTime().AddSeconds(obj);
        }
        /// <summary>
        /// 转换为日期时间(Unix时间戳)
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static DateTime? ToUnixDateTime(this long? obj)
        {
            if (obj.HasValue)
                return new DateTime(621355968000000000).ToLocalTime().AddSeconds(obj.Value);
            return null;
        }
        /// <summary>
        /// 转换为16进制字符串
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToXString(this long obj)
        {
            return obj.ToString("X");
        }
        /// <summary>
        /// 换为16进制字符串
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToXString(this long? obj)
        {
            return obj?.ToString("X");
        }
    }
}
