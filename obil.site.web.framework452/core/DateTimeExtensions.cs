﻿namespace System
{
    /// <summary>
    /// DateTimeExtensions
    /// obil.site.core
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// 转换为Unix时间戳
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static long ToUnixTimestamp(this DateTime obj)
        {
            return (obj.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
        }
        /// <summary>
        /// 转换为Unix时间戳
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static long? ToUnixTimestamp(this DateTime? obj)
        {
            if (obj.HasValue)
                return (obj.Value.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
            return null;
        }
    }
}
