﻿using Newtonsoft.Json;
using System.Linq;

namespace System
{
    /// <summary>
    /// ObjectExtensions
    /// obil.site.core
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// 获取当前值或默认值(当前值为null时)
        /// obil.site.core
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="defualtValue">默认值</param>
        /// <returns></returns>
        public static T GetValueOrDefualt<T>(this T obj, T defualtValue)
        {
            if (obj == null)
                return defualtValue;
            return obj;
        }
        /// <summary>
        /// JSON序列化
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        public static string JsonSerialize(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        /// <summary>
        /// 获取当前对象的克隆
        /// obil.site.core
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T JsonClone<T>(this T obj) where T : class, new()
        {
            return obj.JsonSerialize().JsonDeserialize<T>();
        }
        /// <summary>
        /// 当前值是否在指定集合内
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="objs"></param>
        /// <returns></returns>
        public static bool In(this object obj, params object[] objs)
        {
            return objs.Contains(obj);
        }
    }
}
