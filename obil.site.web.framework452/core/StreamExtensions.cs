﻿using System.IO;
using System.Threading.Tasks;

namespace System
{
    /// <summary>
    /// ObjectExtensions
    /// obil.site.core
    /// </summary>
    public static class StreamExtensions
    {
        /// <summary>
        /// 读取从当前位置到流末尾的所有字符
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ReadToEnd(this Stream obj)
        {
            using (var sr = new StreamReader(obj))
            {
                return sr.ReadToEnd();
            }
        }
        /// <summary>
        /// 读取从当前位置到流末尾的所有字符
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static async Task<string> ReadToEndAsync(this Stream obj)
        {
            using (var sr = new StreamReader(obj))
            {
                return await sr.ReadToEndAsync();
            }
        }
    }
}
