﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace System
{
    /// <summary>
    /// ByteExtensions
    /// obil.site.core
    /// </summary>
    public static class ByteExtensions
    {
        /// <summary>
        /// 转换为文本字符串
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string ToTextString(this byte[] obj, string encoding = "utf-8")
        {
            return Encoding.GetEncoding(encoding).GetString(obj);
        }
        /// <summary>
        /// 换为BASE64字符串
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToBase64String(this byte[] obj)
        {
            return Convert.ToBase64String(obj);
        }
        /// <summary>
        /// MD5加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string MD5Encryption(this byte[] obj)
        {
            using (var sha = new MD5CryptoServiceProvider())
            {
                var hash = sha.ComputeHash(obj);
                return BitConverter.ToString(hash).Replace("-", string.Empty);
            }
        }
        /// <summary>
        /// SHA1加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SHA1Encryption(this byte[] obj)
        {
            using (var sha = new SHA1CryptoServiceProvider())
            {
                var hash = sha.ComputeHash(obj);
                return BitConverter.ToString(hash).Replace("-", string.Empty);
            }
        }
        /// <summary>
        /// SHA1加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SHA256Encryption(this byte[] obj)
        {
            using (var sha = new SHA256CryptoServiceProvider())
            {
                var hash = sha.ComputeHash(obj);
                return BitConverter.ToString(hash).Replace("-", string.Empty);
            }
        }
        /// <summary>
        /// SHA384加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SHA384Encryption(this byte[] obj)
        {
            using (var sha = new SHA384CryptoServiceProvider())
            {
                var hash = sha.ComputeHash(obj);
                return BitConverter.ToString(hash).Replace("-", string.Empty);
            }
        }
        /// <summary>
        /// SHA384加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SHA512Encryption(this byte[] obj)
        {
            using (var sha = new SHA512CryptoServiceProvider())
            {
                var hash = sha.ComputeHash(obj);
                return BitConverter.ToString(hash).Replace("-", string.Empty);
            }
        }
        /// <summary>
        /// AES128加密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="key">密码</param>
        /// <param name="iv">向量</param>
        /// <returns></returns>
        public static byte[] AES128Encryption(this byte[] obj, byte[] key, byte[] iv)
        {
            using (RijndaelManaged rijndaelCipher = new RijndaelManaged())
            {
                rijndaelCipher.Mode = CipherMode.CBC;
                rijndaelCipher.Padding = PaddingMode.PKCS7;
                rijndaelCipher.KeySize = 128;
                rijndaelCipher.BlockSize = 128;
                rijndaelCipher.IV = iv;
                rijndaelCipher.Key = key.Take(16).ToArray();
                ICryptoTransform transform = rijndaelCipher.CreateEncryptor();
                return transform.TransformFinalBlock(obj, 0, obj.Length);
            }
        }
        /// <summary>
        /// AES128解密
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="key">密码</param>
        /// <param name="iv">向量</param>
        /// <returns></returns>
        public static byte[] AES128Decryption(this byte[] obj, byte[] key, byte[] iv)
        {
            using (RijndaelManaged rijndaelCipher = new RijndaelManaged())
            {
                rijndaelCipher.Mode = CipherMode.CBC;
                rijndaelCipher.Padding = PaddingMode.PKCS7;
                rijndaelCipher.KeySize = 128;
                rijndaelCipher.BlockSize = 128;
                rijndaelCipher.IV = iv;
                rijndaelCipher.Key = key.Take(16).ToArray();
                ICryptoTransform transform = rijndaelCipher.CreateDecryptor();
                return transform.TransformFinalBlock(obj, 0, obj.Length);
            }
        }
    }
}
