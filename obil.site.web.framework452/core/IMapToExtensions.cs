﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    /// <summary>
    /// IMapToExtensions
    /// obil.site.coresc
    /// </summary>
    public static class IMapToExtensions
    {
        /// <summary>
        /// 数据映射到
        /// obil.site.coresc
        /// </summary>
        /// <typeparam name="T">目标类型</typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T MapTo<T>(this IMapTo<T> obj) where T : class, new()
        {
            var t = new T();
            obj.MapTo(t);
            return t;
        }
        /// <summary>
        /// 数据映射到
        /// obil.site.coresc
        /// </summary>
        /// <typeparam name="T">目标类型</typeparam>
        /// <param name="obj"></param>
        /// <param name="traget"></param>
        public static void MapTo<T>(this IMapTo<T> obj, out T traget) where T : class, new()
        {
            traget = new T();
            obj.MapTo(traget);
        }
    }
}
