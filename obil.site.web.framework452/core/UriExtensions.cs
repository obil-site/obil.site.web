﻿using System.IO;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    /// <summary>
    /// UriExtensions
    /// obil.site.core
    /// </summary>
    public static class UriExtensions
    {
        /// <summary>
        /// 获取URI的方案和权限段
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetAuthority(this Uri obj)
        {
            return obj.GetLeftPart(UriPartial.Authority);
        }
        /// <summary>
        /// 作为URI下载文件
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="fileName">目标文件路径</param>
        /// <param name="encoding">编码</param>
        public static void DownloadFile(this Uri obj, string fileName, string encoding = "utf-8")
        {
            using (var web = new WebClient())
            {
                ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;

                web.Headers[HttpRequestHeader.UserAgent] = "obil.site.core/1.0";
                web.Encoding = Encoding.GetEncoding(encoding);
                web.DownloadFile(obj, fileName);
            }
        }
        /// <summary>
        /// 作为URI下载文本
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string DownloadString(this Uri obj, string encoding = "utf-8")
        {
            using (var web = new WebClient())
            {
                ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;

                web.Headers[HttpRequestHeader.UserAgent] = $"obil.site.core/{Assembly.GetExecutingAssembly().GetName().Version}";
                web.Encoding = Encoding.GetEncoding(encoding);
                return web.DownloadString(obj);
            }
        }
        /// <summary>
        /// 作为URI下载文本
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static async Task<string> DownloadStringAsync(this Uri obj, string encoding = "utf-8")
        {
            using (var web = new WebClient())
            {
                ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;

                web.Headers[HttpRequestHeader.UserAgent] = $"obil.site.core/{Assembly.GetExecutingAssembly().GetName().Version}";
                web.Encoding = Encoding.GetEncoding(encoding);
                return await web.DownloadStringTaskAsync(obj);
            }
        }
        /// <summary>
        /// 作为URI上传文本
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="body">数据内容</param>
        /// <param name="contentType">数据类型</param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string UploadString(this Uri obj, string body, string contentType = "application/json", string encoding = "utf-8")
        {
            ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;

            using (var web = new WebClient())
            {
                web.Headers[HttpRequestHeader.UserAgent] = $"obil.site.core/{Assembly.GetExecutingAssembly().GetName().Version}";
                web.Encoding = Encoding.GetEncoding(encoding);
                web.Headers.Add(HttpRequestHeader.ContentType, contentType);
                return web.UploadString(obj, body);
            }
        }
        /// <summary>
        /// 作为URI上传文本
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="body">数据内容</param>
        /// <param name="contentType">数据类型</param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static async Task<string> UploadStringAsync(this Uri obj, string body, string contentType = "application/json", string encoding = "utf-8")
        {
            ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;

            using (var web = new WebClient())
            {
                web.Headers[HttpRequestHeader.UserAgent] = $"obil.site.core/{Assembly.GetExecutingAssembly().GetName().Version}";
                web.Encoding = Encoding.GetEncoding(encoding);
                web.Headers.Add(HttpRequestHeader.ContentType, contentType);
                return await web.UploadStringTaskAsync(obj, body);
            }
        }
        /// <summary>
        /// 作为URI上传文本(需要证书)
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="body">数据内容</param>
        /// <param name="certFileName">证书文件路径</param>
        /// <param name="certPassword">证书密钥</param>
        /// <param name="contentType">数据类型</param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string UploadStringWithCert(this Uri obj, string body, string certFileName, string certPassword, string contentType = "application/json", string encoding = "utf-8")
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            X509Certificate2 cer = new X509Certificate2(certFileName, certPassword, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet);
            HttpWebRequest webrequest = WebRequest.CreateHttp(obj);
            webrequest.ClientCertificates.Add(cer);
            webrequest.Method = "POST";
            webrequest.ContentType = contentType;
            var bytes = Encoding.GetEncoding(encoding).GetBytes(body);
            webrequest.ContentLength = bytes.Length;
            webrequest.GetRequestStream().Write(bytes, 0, bytes.Length);
            using (WebResponse webreponse = webrequest.GetResponse())
            {
                using (Stream stream = webreponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }
        /// <summary>
        /// 作为URI上传文本(需要证书)
        /// obil.site.core
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="body">数据内容</param>
        /// <param name="certFileName">证书文件路径</param>
        /// <param name="certPassword">证书密钥</param>
        /// <param name="contentType">数据类型</param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static async Task<string> UploadStringWithCertAsync(this Uri obj, string body, string certFileName, string certPassword, string contentType = "application/json", string encoding = "utf-8")
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            X509Certificate2 cer = new X509Certificate2(certFileName, certPassword, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet);
            HttpWebRequest webrequest = WebRequest.CreateHttp(obj);
            webrequest.ClientCertificates.Add(cer);
            webrequest.Method = "POST";
            webrequest.ContentType = contentType;
            var bytes = Encoding.GetEncoding(encoding).GetBytes(body);
            webrequest.ContentLength = bytes.Length;
            webrequest.GetRequestStream().Write(bytes, 0, bytes.Length);
            using (WebResponse webreponse = await webrequest.GetResponseAsync())
            {
                using (Stream stream = webreponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        return await reader.ReadToEndAsync();
                    }
                }
            }
        }
        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain,
       SslPolicyErrors errors)
        {
            if (errors == SslPolicyErrors.None)
                return true;
            return false;
        }
    }
}
