﻿using System;

namespace Dto
{
    /// <summary>
    /// API返回模型
    /// obil.site.dto
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class ApiResult<TData>
    {
        /// <summary>
        /// 请求结果状态
        /// 1200表示成功
        /// 1400表示已知错误（请直接向用户展示错误描述信息）
        /// 1401表示用户身份验证失败（例如：未登录）
        /// 1403表示权限验证失败（例如：非会员）
        /// 1406表示请求数据验证失败（例如：必填项为空）
        /// 1500表示未知错误（BUG）
        /// obil.site.dto
        /// </summary>
        public int code { get; set; } = 1200;
        /// <summary>
        /// 请求结果描述
        /// obil.site.dto
        /// </summary>
        public string msg { get; set; } = "成功";
        /// <summary>
        /// 请求结果
        /// obil.site.dto
        /// </summary>
        public TData data { get; set; }
        /// <summary>
        /// 当前时间戳
        /// obil.site.dto
        /// </summary>
        public long timestamp { get; set; } = DateTime.Now.ToUnixTimestamp();
    }
}
