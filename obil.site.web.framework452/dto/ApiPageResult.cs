﻿using System.Collections.Generic;

namespace Dto
{
    /// <summary>
    /// API返回模型(分页数据)
    /// obil.site.dto
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class ApiPageResult<TData> : ApiResult<PageData<TData>> { }
    /// <summary>
    /// 分页数据模型
    /// obil.site.dto
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class PageData<TData>
    {
        /// <summary>
        /// 总数据长度
        /// obil.site.dto
        /// </summary>
        public int total { get; set; }
        /// <summary>
        /// 当前页数据
        /// obil.site.dto
        /// </summary>

        public IEnumerable<TData> data { get; set; }
    }
}
