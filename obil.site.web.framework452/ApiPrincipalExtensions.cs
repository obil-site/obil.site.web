﻿using ServiceStack.Redis;
using System;
using System.Security.Principal;

namespace WebApi
{
    /// <summary>
    /// ApiPrincipalExtensions
    /// obil.site.web
    /// </summary>
    public static class ApiPrincipalExtensions
    {
        /// <summary>
        /// 获取Token字符串
        /// 上传方式 Authorization obil {Token}
        /// obil.site.web
        /// </summary>
        /// <param name="principal">principal</param>
        /// <param name="name">用户名</param>
        /// <param name="authCodes">权限集合</param>
        /// <returns></returns>
        public static string GetTokenString(this IPrincipal principal, string name, string authCodes)
        {
            if (principal.Identity is ApiIdentity)
            {
                var apiIdentity = principal.Identity as ApiIdentity;
                apiIdentity.Name = name;
                apiIdentity.AuthCodes = authCodes;
                apiIdentity.Exipre = DateTime.Now.AddDays(WebGlobalConfig.Config.AuthorizationExpire).ToUnixTimestamp();
                var token = apiIdentity.JsonSerialize().AES128Encryption();
                if (WebGlobalConfig.Config.EnableRedisAuthorizationCache)
                {
                    var key = token.MD5Encryption();
                    using (var client = new RedisClient(WebGlobalConfig.Config.RedisConnectionString))
                    {
                        client.Add(key, token, apiIdentity.Exipre.ToUnixDateTime());
                    }
                    token = key;
                }
                apiIdentity.Token = token;
                return token;
            }
            return string.Empty;
        }
    }
}
