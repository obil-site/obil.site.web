﻿using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.OutputCache.Core.Cache;

namespace WebApi
{
    /// <summary>
    /// Redis API缓存器
    /// obil.site.web
    /// </summary>
    public class RedisApiOutputCache : IApiOutputCache
    {
        private readonly IRedisClient _client;
        /// <summary>
        /// RedisApiOutputCache
        /// obil.site.web
        /// </summary>
        /// <param name="host"></param>
        public RedisApiOutputCache(string host)
        {
            _client = new RedisClient(host);
        }
        /// <summary>
        /// Get
        /// obil.site.web
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key) where T : class
        {
            return _client.Get<T>(key);
        }
        /// <summary>
        /// Contains
        /// obil.site.web
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Contains(string key)
        {
            return this.AllKeys.Contains(key);
        }
        /// <summary>
        /// Add
        /// obil.site.web
        /// </summary>
        /// <param name="key"></param>
        /// <param name="o"></param>
        /// <param name="expiration"></param>
        /// <param name="dependsOnKey"></param>
        public void Add(string key, object o, DateTimeOffset expiration, string dependsOnKey = null)
        {

            if (o == null) return;

            var expireTime = expiration.Subtract(DateTimeOffset.Now);


            lock (_client)
            {
                _client.Add(key, o, expireTime);
            }

        }
        /// <summary>
        /// AllKeys
        /// obil.site.web
        /// </summary>
        public IEnumerable<string> AllKeys
        {
            get
            {
                return _client.GetAllKeys();
            }
        }
        #region 不支持
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object Get(string key)
        {
            throw new NotSupportedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            throw new NotSupportedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public void RemoveStartsWith(string key)
        {
            throw new NotSupportedException();
        }
        #endregion
    }
}
