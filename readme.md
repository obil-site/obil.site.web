1. 安装

   | 环境                  | 包                                         |
   | --------------------- | ------------------------------------------ |
   | .net framework 4.8.0  | Nuget Install - obil.site.web              |
   | .net framework 4.5.2  | Nuget Install - obil.site.web.framework452 |

2. 配置

   ```c#
   //引用
   using WebApi;
   
   public class WebApiApplication : System.Web.HttpApplication
       {
           protected void Application_Start()
           {
               AreaRegistration.RegisterAllAreas();
               GlobalConfiguration.Configure(WebApiConfig.Register);
               FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
               RouteConfig.RegisterRoutes(RouteTable.Routes);
               BundleConfig.RegisterBundles(BundleTable.Bundles);
   
               //注册配置
               WebGlobalConfig.Register();
               //这里开始配置(不配置则使用默认值)
               WebGlobalConfig.Config.AuthorizationExpire = 7; //授权有效期
               WebGlobalConfig.Config.EnableRedisApiOutputCache = false; //Redis 缓存接口数据
               WebGlobalConfig.Config.EnableRedisAuthorizationCache = false; //Redis 缓存授权信息
               WebGlobalConfig.Config.RedisConnectionString = ""; // Redis 连接字符串
               WebGlobalConfig.Config.OnException += Config_OnException; //异常事件
               WebGlobalConfig.Config.OnDatabaseExecute += Config_OnDatabaseExecute; //数据库事件
               WebGlobalConfig.Config.OnWebApiResponsed += Config_OnWebApiResponsed; //接口事件
           }
   
           private void Config_OnWebApiResponsed(object sender, OnWebApiResponsedEventArgs e)
           {
               //接口监测
               //throw new NotImplementedException();
           }
   
           private void Config_OnDatabaseExecute(object sender, OnDatabaseExecuteEventArgs e)
           {
               //数据库监测
               //throw new NotImplementedException();
           }
   
           private void Config_OnException(object sender, OnExceptionEventArgs e)
           {
               //异常处理
               //throw new NotImplementedException();
           }
       }
   ```

3. 接口代码

   ```c#
   //引用
   using Dto;
   using WebApi;
   ....
   [RoutePrefix("api/userinfo")]
   public class UserInfoController : ApiController
       {
           [HttpPost]
           [HttpOptions]//跨域支持(结合WebConfig配置)
           [Route("login")]
           public async System.Threading.Tasks.Task<ApiResult<string>> LoginAsync([FromBody] LoginIpt ipt)
           {
               using (var builder = new ApiResultBuilder<string, DbEntity>(string.Empty))
               {
                   var user = await builder.Database.UserInfo
                   .FirstOrDefualtAsync(d => d.Account == ipt.Account && d.Password = ipt.Password);
                   if (user == null) return builder.Error1400("账户或密码不正确");
                   builder.data = User.GetTokenString(user.Account, user.AuthCodes);//授权信息包含账号和权限信息
                   return builder.Successed();
               }
           }
           [HttpGet]
           [HttpOptions]//跨域支持(结合WebConfig配置)
           [Route("tel")]
           [ApiIdentityFilter("A001")]//权限及登录验证
           [CacheOutput(ServerTimeSpan =60,ClientTimeSpan =60)]//接口缓存
           public async System.Threading.Tasks.Task<ApiResult<string>> TelAsync()
           {
               using (var builder = new ApiResultBuilder<string, DbEntity>(string.Empty))
               {
                   var user = await builder.Database.UserInfo.FirstOrDefualtAsync(d => d.Account == User.Identity.Name);
                   if (user == null) return builder.Error1400("用户已删除");
                   builder.data = user.Tel;
                   return builder.Successed();
               }
           }
   }
   public class LoginIpt
   {
           public string Account { get; set; }
           public string Password { get; set; }
   }
   ```

4. 接口请求

   ```
   - api/userinfo/login
   - 请求
   {
       "Account":"admin",
       "Password":"e10adc3949ba59abbe56e057f20f883e"
   }
   - 返回
   {
   	"code": 1200,
   	"msg": "成功",
   	"data": "k7bCP8/Mz8Lst5eTwxF6rw==",//如果开启 Redis 权限信息 缓存 , 这里返回固定长度值(MD5)
   	"timestamp": 1619885877
   }
   - 返回 失败 错误
   {
   	"code": 1400,
   	"msg": "账户或密码不正确",
   	"data": "",
   	"timestamp": 1619885877
   }
   ```

   ```
   - api/userinfo/tel
   - 请求
   无
   - 头部
   Header:Authorization = "Obil k7bCP8/Mz8Lst5eTwxF6rw=="
   - 返回
   {
   	"code": 1200,
   	"msg": "成功",
   	"data": "13000000000",
   	"timestamp": 1619885877
   }
   - 返回 失败 未登录
   {
   	"code": 1401,
   	"msg": "未登录",
   	"data": "",
   	"timestamp": 1619885877
   }
   - 返回 失败 无权限
   {
   	"code": 1403,
   	"msg": "无权限",
   	"data": "",
   	"timestamp": 1619885877
   }
   - 返回 失败 错误
   {
   	"code": 1400,
   	"msg": "用户已删除",
   	"data": "",
   	"timestamp": 1619885877
   }
   
   ```

   