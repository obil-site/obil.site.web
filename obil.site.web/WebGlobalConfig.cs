﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Mvc;
using WebApi.OutputCache.V2;

namespace WebApi
{
    /// <summary>
    /// 全局配置
    /// obil.site.web
    /// </summary>
    public class WebGlobalConfig
    {
        /// <summary>
        /// 配置信息
        /// obil.site.web
        /// </summary>
        public static WebGlobalConfig Config { get; private set; }
        /// <summary>
        /// 全局注册(默认配置)
        /// obil.site.web
        /// </summary>
        public static void Register()
        {
            Register(new WebGlobalConfig());
        }
        /// <summary>
        /// 全局注册
        /// obil.site.web
        /// </summary>
        /// <param name="webGlobalConfig">WebGlobalConfig</param>
        public static void Register(WebGlobalConfig webGlobalConfig)
        {
            Config = webGlobalConfig;

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters, GlobalConfiguration.Configuration.Filters);

            FormatterConfig.RegisterGlobalFormatters(GlobalConfiguration.Configuration.Formatters);

            if (webGlobalConfig.EnableRedisApiOutputCache)
            {
                GlobalConfiguration.Configuration.CacheOutputConfiguration()
               .RegisterCacheOutputProvider(() => new RedisApiOutputCache(webGlobalConfig.RedisConnectionString));
            }
        }
        /// <summary>
        /// 授权信息有效期(天)(默认:7天)
        /// obil.site.web
        /// </summary>
        public int AuthorizationExpire { get; set; } = 7;
        /// <summary>
        /// 开启授权信息 Redis 缓存,开启将使用短授权信息(默认:False)
        /// obil.site.web
        /// </summary>
        public bool EnableRedisAuthorizationCache { get; set; }
        /// <summary>
        /// 是否开启WEBAPI Redis 缓存(默认:False)
        /// obil.site.web
        /// </summary>
        public bool EnableRedisApiOutputCache { get; set; }
        /// <summary>
        /// Redis 连接字符串
        /// obil.site.web
        /// </summary>
        public string RedisConnectionString { get; set; }
        /// <summary>
        /// 当异常发生时触发
        /// obil.site.web
        /// </summary>
        public event EventHandler<OnExceptionEventArgs> OnException;
        /// <summary>
        /// 当EF执行SQL命令时触发
        /// obil.site.web
        /// </summary>
        public event EventHandler<OnDatabaseExecuteEventArgs> OnDatabaseExecute;
        /// <summary>
        /// 当WEBAPI成功返回时触发
        /// obil.site.web
        /// </summary>
        public event EventHandler<OnWebApiResponsedEventArgs> OnWebApiResponsed;

        internal void TriggerOnException(Exception exception) { OnException?.Invoke(this, new OnExceptionEventArgs(exception)); }
        internal void TriggerOnDatabaseExecute(string sql) { OnDatabaseExecute?.Invoke(this, new OnDatabaseExecuteEventArgs(sql)); }
        internal void TriggerOnWebApiResponsed(WebApiInfo webApiInfo) { OnWebApiResponsed?.Invoke(this, new OnWebApiResponsedEventArgs(webApiInfo)); }
    }
    /// <summary>
    /// OnExceptionEventArgs
    /// obil.site.web
    /// </summary>
    public class OnExceptionEventArgs : EventArgs
    {
        /// <summary>
        /// 异常信息
        /// </summary>
        public Exception Exception { get; set; }
        /// <summary>
        /// OnExceptionEventArgs
        /// obil.site.web
        /// </summary>
        /// <param name="exception"></param>
        public OnExceptionEventArgs(Exception exception)
        {
            Exception = exception;
        }
    }
    /// <summary>
    /// OnDatabaseExecuteEventArgs
    /// obil.site.web
    /// </summary>
    public class OnDatabaseExecuteEventArgs : EventArgs
    {
        /// <summary>
        /// SQL命令
        /// obil.site.web
        /// </summary>
        public string Sql { get; set; }
        /// <summary>
        /// OnDatabaseExecuteEventArgs
        /// obil.site.web
        /// </summary>
        /// <param name="sql"></param>
        public OnDatabaseExecuteEventArgs(string sql)
        {
            Sql = sql;
        }
    }
    /// <summary>
    /// OnWebApiResponsedEventArgs
    /// obil.site.web
    /// </summary>
    public class OnWebApiResponsedEventArgs : EventArgs
    {
        /// <summary>
        /// 接口信息
        /// obil.site.web
        /// </summary>
        public WebApiInfo WebApiInfo { get; set; }
        /// <summary>
        /// OnWebApiResponsedEventArgs
        /// obil.site.web
        /// </summary>
        /// <param name="webApiInfo"></param>
        public OnWebApiResponsedEventArgs(WebApiInfo webApiInfo)
        {
            WebApiInfo = webApiInfo;
        }
    }
    /// <summary>
    /// 接口信息
    /// obil.site.web
    /// </summary>
    public class WebApiInfo
    {
        /// <summary>
        /// 请求方法
        /// obil.site.web
        /// </summary>
        public string HttpMethod { get; set; }
        /// <summary>
        /// 绝对路径
        /// obil.site.web
        /// </summary>
        public string AbsolutePath { get; set; }
        /// <summary>
        /// 授权用户名
        /// obil.site.web
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 客户IP地址
        /// obil.site.web
        /// </summary>
        public string UserIPAddress { get; set; }
        /// <summary>
        /// 浏览器信息
        /// obil.site.web
        /// </summary>
        public string UserAgent { get; set; }
        /// <summary>
        /// 请求参数
        /// obil.site.web
        /// </summary>
        public IDictionary<string, object> ActionArguments { get; set; }
        /// <summary>
        /// 返回数据
        /// obil.site.web
        /// </summary>
        public object ResultContent { get; set; }
        /// <summary>
        /// 请求时间(发起)
        /// obil.site.web
        /// </summary>
        public DateTime RequestTime { get; set; }
    }

}
