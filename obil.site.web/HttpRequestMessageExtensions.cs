﻿using System.Net.Http;
using System.ServiceModel.Channels;

namespace WebApi
{
    /// <summary>
    /// HttpRequestMessageExtensions
    /// obil.site.web
    /// </summary>
    public static class HttpRequestMessageExtensions
    {
        /// <summary>
        /// 获取客户端IP地址
        /// obil.site.web
        /// </summary>
        /// <param name="httpRequest"></param>
        /// <returns></returns>
        public static string GetClientIPAddress(this HttpRequestMessage httpRequest)
        {
            if (httpRequest.Properties.ContainsKey(MSHttpContextProperty.Name))
            {
                dynamic ctx = httpRequest.Properties[MSHttpContextProperty.Name];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }
            if (httpRequest.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                dynamic remoteEndpoint = httpRequest.Properties[RemoteEndpointMessageProperty.Name];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }
            return "0.0.0.0";
        }
    }
    /// <summary>
    /// MSHttpContextProperty
    /// obil.site.web
    /// </summary>
    public static class MSHttpContextProperty
    {
        /// <summary>
        /// 
        /// </summary>
        public static string Name { get; } = "MS_HttpContext";
    }
}
