﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;

namespace WebApi
{
    /// <summary>
    /// FormatterConfig
    /// obil.site.web
    /// </summary>
    public class FormatterConfig
    {
        /// <summary>
        /// 全局序列化注册
        /// obil.site.web
        /// </summary>
        /// <param name="formatters">GlobalConfiguration.Configuration.Formatters</param>
        internal static void RegisterGlobalFormatters(MediaTypeFormatterCollection formatters)
        {
            formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);

            formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            formatters.JsonFormatter.SerializerSettings.DateFormatHandling = DateFormatHandling.MicrosoftDateFormat;
            formatters.JsonFormatter.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";

            formatters.JsonFormatter.SerializerSettings.ContractResolver = new ApiJsonContractResolver();
        }
    }
    /// <summary>
    /// ApiJsonContractResolver
    /// obil.site.web
    /// </summary>
    public class ApiJsonContractResolver : DefaultContractResolver
    {
        /// <summary>
        /// CreateProperties
        /// obil.site.web
        /// </summary>
        /// <param name="type"></param>
        /// <param name="memberSerialization"></param>
        /// <returns></returns>
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return type.GetProperties()
                   .Select(p =>
                   {
                       var jp = base.CreateProperty(p, memberSerialization);

                       var CustomAttributes = p.GetCustomAttributes(true);

                       jp.ValueProvider = new JsonDefaultValueProvider(p);//默认值

                       var dateTimeFormat = CustomAttributes?.FirstOrDefault(q => q is JsonDateTimeFormatAttribute) as JsonDateTimeFormatAttribute;

                       if (dateTimeFormat != null)
                           jp.Converter = new IsoDateTimeConverter() { DateTimeFormat = dateTimeFormat.Format };//日期格式化

                       return jp;
                   }).ToList();
        }
    }
    /// <summary>
    /// 返回日期格式
    /// obil.site.web
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class JsonDateTimeFormatAttribute : Attribute
    {
        /// <summary>
        /// Format
        /// obil.site.web
        /// </summary>
        public string Format { get; set; }
        /// <summary>
        /// format
        /// obil.site.web
        /// </summary>
        /// <param name="format"></param>
        public JsonDateTimeFormatAttribute(string format)
        {
            Format = format;
        }
    }
    /// <summary>
    /// 返回默认值
    /// obil.site.web
    /// </summary>
    public class JsonDefaultValueProvider : IValueProvider
    {
        private PropertyInfo MemberInfo { get; set; }
        /// <summary>
        /// JsonDefaultValueProvider
        /// obil.site.web
        /// </summary>
        /// <param name="memberInfo"></param>
        public JsonDefaultValueProvider(PropertyInfo memberInfo)
        {
            MemberInfo = memberInfo;
        }
        /// <summary>
        /// GetValue
        /// obil.site.web
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public object GetValue(object target)
        {
            object result = MemberInfo.GetValue(target, null);
            if (result == null)
            {
                if (MemberInfo.PropertyType == typeof(string))
                    return string.Empty;
                if (MemberInfo.PropertyType.IsArray)
                    return Array.CreateInstance(MemberInfo.PropertyType.GetElementType(), 0);
                if (MemberInfo.PropertyType.GetConstructor(Type.EmptyTypes) != null)
                    return Activator.CreateInstance(MemberInfo.PropertyType);
            }
            return result;
        }
        /// <summary>
        /// SetValue
        /// obil.site.web
        /// </summary>
        /// <param name="target"></param>
        /// <param name="value"></param>
        public void SetValue(object target, object value)
        {
            MemberInfo.SetValue(target, value, null);
        }
    }

}
