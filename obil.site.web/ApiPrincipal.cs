﻿using System;
using System.Security.Principal;

namespace WebApi
{
    /// <summary>
    /// 授权信息
    /// obil.site.web
    /// </summary>
    public class ApiPrincipal : IPrincipal
    {
        /// <summary>
        /// Identity
        /// obil.site.web
        /// </summary>
        public IIdentity Identity { get; set; }
        /// <summary>
        /// IsInRole
        /// obil.site.web
        /// </summary>
        /// <param name="authCode"></param>
        /// <returns></returns>
        public bool IsInRole(string authCode)
        {
            if (string.IsNullOrWhiteSpace(authCode))
                return true;

            if (Identity is ApiIdentity)
            {
                var apiIdentity = Identity as ApiIdentity;

                if (string.IsNullOrWhiteSpace(apiIdentity.AuthCodes))
                    return false;

                return apiIdentity.AuthCodes.Contains(authCode);
            }

            return false;
        }


    }
    /// <summary>
    /// 身份信息
    /// obil.site.web
    /// </summary>
    public class ApiIdentity : IIdentity
    {
        /// <summary>
        /// Name
        /// obil.site.web
        /// </summary>
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// AuthCodes
        /// obil.site.web
        /// </summary>
        public string AuthCodes { get; set; } = string.Empty;
        /// <summary>
        /// AuthenticationType = obil
        /// obil.site.web
        /// </summary>

        public string AuthenticationType { get; private set; } = "obil";
        /// <summary>
        /// IsAuthenticated
        /// obil.site.web
        /// </summary>

        public bool IsAuthenticated { get { return !string.IsNullOrWhiteSpace(Name); } }
        /// <summary>
        /// Token
        /// obil.site.web
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 有效期(秒)
        /// obil.site.web
        /// </summary>
        public long Exipre { get; set; }
        /// <summary>
        /// 请求发起时间
        /// obil.site.web
        /// </summary>
        public DateTime RequestTime { get; set; } = DateTime.Now;
    }
    /// <summary>
    /// ApiIdentityFilterAttribute
    /// obil.site.web
    /// </summary>

    public class ApiIdentityFilterAttribute : Attribute
    {
        /// <summary>
        /// 权限代码
        /// obil.site.web
        /// </summary>
        public string AuthCode { get; set; }
        /// <summary>
        /// ApiIdentityFilterAttribute
        /// obil.site.web
        /// </summary>
        public ApiIdentityFilterAttribute() { }
        /// <summary>
        /// ApiIdentityFilterAttribute
        /// obil.site.web
        /// </summary>
        /// <param name="authCode">authCode</param>
        public ApiIdentityFilterAttribute(string authCode) { AuthCode = authCode; }
    }
}
