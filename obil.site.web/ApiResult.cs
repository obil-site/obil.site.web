﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace WebApi
{
    /// <summary>
    /// API返回模型
    ///obil.site.web
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class ApiResult<TData>
    {
        /// <summary>
        /// 请求结果状态
        /// 1200表示成功
        /// 1400表示已知错误（请直接向用户展示错误描述信息）
        /// 1401表示用户身份验证失败（例如：未登录）
        /// 1403表示权限验证失败（例如：非会员）
        /// 1406表示请求数据验证失败（例如：必填项为空）
        /// 1500表示未知错误（BUG）
        ///obil.site.web
        /// </summary>
        public int code { get; set; } = 1200;
        /// <summary>
        /// 请求结果描述
        ///obil.site.web
        /// </summary>
        public string msg { get; set; } = "成功";
        /// <summary>
        /// 请求结果
        ///obil.site.web
        /// </summary>
        public TData data { get; set; }
        /// <summary>
        /// 当前时间戳
        ///obil.site.web
        /// </summary>
        public long timestamp { get; set; } = DateTime.Now.ToUnixTimestamp();
    }
    /// <summary>
    /// API返回模型(分页数据)
    ///obil.site.web
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class ApiPageResult<TData> : ApiResult<PageData<TData>> { }
    /// <summary>
    /// 分页数据模型
    ///obil.site.web
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class PageData<TData>
    {
        /// <summary>
        /// 总数据长度
        ///obil.site.web
        /// </summary>
        public int total { get; set; }
        /// <summary>
        /// 当前页数据
        ///obil.site.web
        /// </summary>

        public IEnumerable<TData> data { get; set; }
    }
    /// <summary>
    /// 结果生成器
    /// obil.site.web
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class ApiResultBuilder<TData> : IDisposable
    {
        /// <summary>
        /// data
        /// obil.site.web
        /// </summary>
        public TData data { get; set; }
        /// <summary>
        /// ApiResultBuilder
        /// obil.site.web
        /// </summary>
        public ApiResultBuilder()
        {

        }
        /// <summary>
        /// ApiResultBuilder
        /// obil.site.web
        /// </summary>
        /// <param name="data"></param>
        public ApiResultBuilder(TData data)
        {
            this.data = data;
        }
        /// <summary>
        /// Dispose
        /// obil.site.web
        /// </summary>
        public virtual void Dispose()
        {

        }
        /// <summary>
        /// 1200表示成功
        /// obil.site.web
        /// </summary>
        /// <returns></returns>
        public ApiResult<TData> Successed()
        {
            return new ApiResult<TData>() { code = 1200, msg = "成功", data = data };
        }
        /// <summary>
        /// 1400表示已知错误（请直接向用户展示错误描述信息）
        /// obil.site.web
        /// </summary>
        /// <param name="msg">错误描述</param>
        /// <returns></returns>
        public ApiResult<TData> Error1400(string msg)
        {
            return new ApiResult<TData>() { code = 1400, msg = msg };
        }
        /// <summary>
        /// 1401表示用户身份验证失败（例如：未登录）
        /// obil.site.web
        /// </summary>
        /// <returns></returns>
        public ApiResult<TData> Error1401()
        {
            return new ApiResult<TData>() { code = 1401, msg = "未登录" };
        }
        /// <summary>
        /// 1403表示权限验证失败（例如：非会员）
        /// obil.site.web
        /// </summary>
        /// <returns></returns>
        public ApiResult<TData> Error1403()
        {
            return new ApiResult<TData>() { code = 1403, msg = "没有相关权限" };
        }
        /// <summary>
        /// 1406表示请求数据验证失败（例如：必填项为空）
        /// obil.site.web
        /// </summary>
        /// <param name="msg">错误描述</param>
        /// <returns></returns>
        public ApiResult<TData> Error1406(string msg)
        {
            return new ApiResult<TData>() { code = 1406, msg = msg };
        }
    }
    /// <summary>
    /// 结果(分页)生成器
    /// obil.site.web
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class ApiPageResultBuilder<TData> : IDisposable
    {
        /// <summary>
        /// data
        /// obil.site.web
        /// </summary>
        public PageData<TData> data { get; set; }
        /// <summary>
        /// TakeSize
        /// obil.site.web
        /// </summary>
        public int TakeSize { get; private set; }
        /// <summary>
        /// SkipSize
        /// obil.site.web
        /// </summary>
        public int SkipSize { get; private set; }
        /// <summary>
        /// 分页
        /// obil.site.web
        /// </summary>
        /// <param name="limit">单页上限(0表示不限制)</param>
        /// <param name="offset">起始位置</param>
        public ApiPageResultBuilder(int limit, int offset)
        {
            TakeSize = limit > 0 ? limit : int.MaxValue;
            SkipSize = offset > 0 ? offset : 0;
            data = new PageData<TData>();
        }
        /// <summary>
        /// Dispose
        /// obil.site.web
        /// </summary>
        public virtual void Dispose()
        {

        }
        /// <summary>
        /// 1200表示成功
        /// obil.site.web
        /// </summary>
        /// <returns></returns>
        public ApiPageResult<TData> Successed()
        {
            return new ApiPageResult<TData>() { code = 1200, msg = "成功", data = data };
        }
        /// <summary>
        /// 1400表示已知错误（请直接向用户展示错误描述信息）
        /// obil.site.web
        /// </summary>
        /// <param name="msg">错误描述</param>
        /// <returns></returns>
        public ApiPageResult<TData> Error1400(string msg)
        {
            return new ApiPageResult<TData>() { code = 1400, msg = msg };
        }
        /// <summary>
        /// 1401表示用户身份验证失败（例如：未登录）
        /// obil.site.web
        /// </summary>
        /// <returns></returns>
        public ApiPageResult<TData> Error1401()
        {
            return new ApiPageResult<TData>() { code = 1401, msg = "未登录" };
        }
        /// <summary>
        /// 1403表示权限验证失败（例如：非会员）
        /// obil.site.web
        /// </summary>
        /// <returns></returns>
        public ApiPageResult<TData> Error1403()
        {
            return new ApiPageResult<TData>() { code = 1403, msg = "没有相关权限" };
        }
        /// <summary>
        /// 1406表示请求数据验证失败（例如：必填项为空）
        /// obil.site.web
        /// </summary>
        /// <param name="msg">错误描述</param>
        /// <returns></returns>
        public ApiPageResult<TData> Error1406(string msg)
        {
            return new ApiPageResult<TData>() { code = 1406, msg = msg };
        }
    }
    /// <summary>
    /// 结果生成器
    /// obil.site.web
    /// </summary>
    /// <typeparam name="TData">TData</typeparam>
    /// <typeparam name="TDb">数据库实例(EF)</typeparam>
    public class ApiResultBuilder<TData, TDb> : ApiResultBuilder<TData>
        where TDb : DbContext, new()
    {
        /// <summary>
        /// 数据库实例(EF)
        /// obil.site.web
        /// </summary>
        public TDb Database { get; set; }
        /// <summary>
        /// ApiResultBuilder
        /// obil.site.web
        /// </summary>
        public ApiResultBuilder() : base()
        {
            Database = new TDb();
            Database.Database.Log = (sql) =>
            {
                if (string.IsNullOrWhiteSpace(sql)) return;
                WebGlobalConfig.Config.TriggerOnDatabaseExecute(sql);
            };
        }
        /// <summary>
        /// ApiResultBuilder
        /// obil.site.web
        /// </summary>
        /// <param name="data"></param>
        public ApiResultBuilder(TData data) : base(data)
        {
            Database = new TDb();
            Database.Database.Log = (sql) =>
            {
                if (string.IsNullOrWhiteSpace(sql)) return;
                WebGlobalConfig.Config.TriggerOnDatabaseExecute(sql);
            };
        }
        /// <summary>
        /// Dispose
        /// obil.site.web
        /// </summary>
        public override void Dispose()
        {
            Database?.Dispose();
            base.Dispose();
        }
    }
    /// <summary>
    /// 结果生成器(分页)
    /// obil.site.web
    /// </summary>
    /// <typeparam name="TData">TData</typeparam>
    /// <typeparam name="TDb">数据库实例(EF)</typeparam>
    public class ApiPageResultBuilder<TData, TDb> : ApiPageResultBuilder<TData>
        where TDb : DbContext, new()
    {
        /// <summary>
        /// 数据库实例(EF)
        /// obil.site.web
        /// </summary>
        public TDb Database { get; set; }
        /// <summary>
        /// ApiPageResultBuilder
        /// obil.site.web
        /// </summary>
        /// <param name="limit">单页上限(0表示不限制)</param>
        /// <param name="offset">起始位置</param>
        public ApiPageResultBuilder(int limit, int offset) : base(limit, offset)
        {
            Database = new TDb();
        }
        /// <summary>
        /// Dispose
        /// obil.site.web
        /// </summary>
        public override void Dispose()
        {
            Database?.Dispose();
            base.Dispose();
        }
    }
}
